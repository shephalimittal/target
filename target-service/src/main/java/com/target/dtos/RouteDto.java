package com.target.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteDto {

	@JsonProperty("Description")
	private String Description;
	
	@JsonProperty("ProviderID")
	private String ProviderID;
	
	@JsonProperty("Route")
	private String Route;

	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public String getProviderID() {
		return ProviderID;
	}

	public void setProviderID(String ProviderID) {
		this.ProviderID = ProviderID;
	}

	public String getRoute() {
		return Route;
	}

	public void setRoute(String Route) {
		this.Route = Route;
	}

}
