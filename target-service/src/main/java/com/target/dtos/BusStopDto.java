package com.target.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusStopDto {

	@JsonProperty("Text")
	private String Text;
	@JsonProperty("Value")
	private String Value;

	public String getText() {
		return Text;
	}

	public void setText(String text) {
		Text = text;
	}

	public String getValue() {
		return Value;
	}

	public void setValue(String value) {
		Value = value;
	}

}
