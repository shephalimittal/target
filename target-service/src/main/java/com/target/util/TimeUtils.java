/**
 * 
 */
package com.target.util;

public class TimeUtils {

	public static Double getTimeDiff(long nextBusTime) {
		long currentTimeinMiilis = System.currentTimeMillis();
		Double minutes = (double) ((nextBusTime - currentTimeinMiilis) / 60000);
		return minutes;
	}
}
